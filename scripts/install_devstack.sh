#!/bin/bash -x

# reference: https://docs.openstack.org/devstack/latest/

user="vagrant"

function run_as_user() {
    sudo su $user -c "${@}"
}

source_dir="~/src/"
devstack_dir="${source_dir}/devstack/"

run_as_user "mkdir -p $source_dir"
run_as_user "git clone https://git.openstack.org/openstack-dev/devstack ${devstack_dir}"

run_as_user "cp /tmp/local.conf ${devstack_dir}"
run_as_user "cd $devstack_dir; ./stack.sh"
