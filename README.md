## Installation

1. install vagrant by following these [directions](https://www.vagrantup.com/intro/getting-started/)
2. `git clone git@gitlab.com:grawk/vagrant-devstack.git`
3. cd vagrant-devstack
4. vagrant up

## Getting into devstack env

1. cd vagrant-devstack
2. vagrant ssh

## Using devstack

* openrc file is located in ~/src/vagrant-devstack/openrc
* to source as admin run `source ~/src/vagrant-devstack/openrc admin`
* to see all openstack services run `systemctl`
* to view logs for a service run `sudo journalctl -u {name_of_service}`
